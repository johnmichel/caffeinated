module.exports = {
    "extends": ["innards", "plugin:react/recommended"],
    "globals": {
        "R": false
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "array-bracket-newline": [2, { "multiline": true, "minItems": 2 }],
        "class-methods-use-this": 0,
        "id-length": [0, {"exceptions": ["R"]}],
        "max-lines": 0,
        "no-magic-numbers": 0
    }
};