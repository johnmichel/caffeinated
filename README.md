# ☕️ Caffeinated


[![](https://img.shields.io/badge/license-MIT-1D7FC4.svg?style=flat-square)](https://gitlab.com/johnmichel/caffeinated/raw/master/LICENSE)
[![](https://img.shields.io/badge/built--with-React-61dafb.svg?style=flat-square)](https://facebook.github.io/react/)
[![](https://img.shields.io/badge/%CE%BB%20with-ramda-884499.svg?style=flat-square)](http://ramdajs.com)
[![](https://img.shields.io/badge/%F0%9F%8E%A8%20with-stylus-ff6347.svg?style="flat-square")](http://stylus-lang.com)
[![](https://img.shields.io/badge/%E2%9C%A8%20with-eslint-3a33d1.svg?style=flat-square)](http://eslint.org)


`$ npm i && npm run start`

![](https://imgg.es/bestcoffee.gif)

![](https://imgg.es/coffeeisready.gif)

![](https://imgg.es/coffee.gif)