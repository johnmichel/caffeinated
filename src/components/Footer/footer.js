import {Link} from 'react-router-dom';
import React from 'react';

const Footer = () =>
    <footer>
        <code><Link to="https://gitlab.com/johnmichel/caffeinated" target="_blank">Source</Link></code>
    </footer>;

export default Footer
