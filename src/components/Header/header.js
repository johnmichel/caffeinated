import {Link} from 'react-router-dom';
import React from 'react';
import logo from '../../images/coffee_cup.svg';

const Header = () =>
    <header>
        <Link to="/caffeinated" className="logo"><img src={logo} className="logo" alt="logo" /></Link>
        <h1><Link to="/caffeinated">I&apos;m Caffeinated</Link></h1>
    </header>;

export default Header
