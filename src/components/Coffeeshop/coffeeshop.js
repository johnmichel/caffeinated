import {Route, Switch} from 'react-router-dom';
import Barista from '../Barista/barista';
import DrinkList from '../Drinklist/drinklist';
import React from 'react';

const CoffeeShop = () =>
    <section>
        <h2 className="App-intro">
            Let&apos;s see why
        </h2>
        <Switch>
            <Route exact path="/caffeinated" component={DrinkList} />
            <Route path="/caffeinated/:number" component={Barista} />
        </Switch>
    </section>;

export default CoffeeShop;