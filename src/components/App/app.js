import '../../main.css';
import CoffeeShop from '../Coffeeshop/coffeeshop';
import Footer from '../Footer/footer';
import Header from '../Header/header';
import React from 'react';

const App = () => (
    <div>
        <Header />
        <CoffeeShop />
        <Footer />
    </div>
);

export default App;
