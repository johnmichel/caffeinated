import {Link} from 'react-router-dom';
import data from '../../data/coffee.json';
import React from 'react';

const R = require('ramda');

const extractDates = R.compose(
    R.reverse,
    R.pluck('date'),
    R.propOr({}, 'coffee')
)(data);

const DrinkList = () =>
    <ul>
        {
            extractDates.map(drinkData => {
                const slug = `/caffeinated/${drinkData}`;

                return <li key={drinkData}>
                    <Link to={slug}>{drinkData}</Link>
                </li>
            })
        }
    </ul>;

export default DrinkList;
