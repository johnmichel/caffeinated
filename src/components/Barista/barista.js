import data from '../../data/coffee.json';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';

const moment = require('moment');
const R = require('ramda');

const extractEntries = function extractEntries() {
    const entries = R.compose(
        R.reverse,
        R.propOr({}, 'coffee')
    )(data);

    return entries;
};

const Barista = props => {
    const extractedEntries = extractEntries();
    const entries = R.compose(
        R.head,
        R.pluck('drinks'),
        R.filter(
            R.propEq('date', props.match.params.number)
        )
    )(extractedEntries);

    const humanizedDate = moment(props.match.params.number, 'YYYYMMDD').format('MMMM D, YYYY');

    const transcribe = entry => `I had ${entry.amount}oz of ${entry.variety} from ${entry.roaster} at ${entry.time} in ${entry.location} and it was enjoyed out of a ${entry.container}.  The brewing method was ${entry.method}.`;

    let content;
    if (entries.length > 1) {
        content = R.compose(
            R.join(' '),
            R.intersperse('Then, '),
            R.map(transcribe)
        )(entries);
    }
    else {
        content = transcribe(R.head(entries));
    }

    return (
        <section className="barista">
            <h3>{humanizedDate}</h3>
            <p>
                {content}
            </p>
            <Link to="/caffeinated">Go Back</Link>
        </section>
    )
};

Barista.propTypes = {
    data: PropTypes.object
};

export default Barista;