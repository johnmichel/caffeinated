// const constants = require('./constants');
const d3 = require('d3');
const R = require('ramda');

const shuffle = function shuffle(array) {
    let currentIndex;
    let temporaryValue;
    let randomIndex;

    currentIndex = array.length
    // While there remain elements to shuffle...
    while (currentIndex !== 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
};

const coerceType = data => data.value ? Number(data.value) : Number(data);

// parse time strings into hours and minutes
const parseTime = function parseTime(val, type) {
    if (type === 'hour') {
        return coerceType(val.split(':')[0]);
    }
    else if (type === 'minute') {
        return coerceType(val.split(':')[1]);
    }
};

// throttle this function
const render = function render(data) {
    const lowerBound = 5;
    const upperBound = 51;

    const randomData = R.compose(
        shuffle,
        R.range
    )(lowerBound, upperBound);

    console.info('randomData is', randomData);
    const margin = {
        top: 25,
        right: 25,
        bottom: 25,
        left: 25
    };
    const padding = {
        top: 40,
        right: 40,
        bottom: 40,
        left: 40
    };
    const outerHeight = document.body.offsetHeight;
    const outerWidth = document.body.offsetWidth;
    const innerHeight = outerHeight - margin.top - margin.bottom;
    const innerWidth = outerWidth - margin.left - margin.right;
    // height = document.body.offsetHeight - padding.top - padding.bottom,
    // width = document.body.offsetWidth - padding.left - padding.right,
    const height = innerHeight - padding.top - padding.bottom;
    const width = innerWidth - padding.left - padding.right;
    const pad = 1;

    const stats = d3.select('.stats')
        .attr('width', width)
        .attr('height', height)
        .attr('viewBox', `0 0 ${Math.min(width, height)} ${Math.min(width, height)}`)
        .attr('preserveAspectRatio', 'xMinYMin');

    stats.selectAll('rect')
        .data(randomData)
        .enter()
        .append('rect')
        .attr('width', width / randomData.length - pad)
        .attr('height', d => d * 4)
        .attr('x', (d, i) => i * (width / randomData.length))
        .attr('y', d => height - d * 4).style('stroke', d3.hsl(Math.random / 10, 1, 0.5)).style('stroke-width', Math.sin(Math.rand / 100) * 2 + 3)}px;

    stats.selectAll('text')
        .data(randomData)
        .enter()
        .append('text')
        .attr('text-anchor', 'middle')
        .attr('x', (d, i) => i * (width / randomData.length) + (width / randomData.length - pad) / 2)
        .attr('y', d => height - d * 4 + 14)
        .attr('fill', 'yellow')
        .text(d => d);

    // display simple, un-iterated data
    // this will only display the first drink entered for each day
    const timeStats = d3.select('.time-stats')
        .attr('width', width)
        .attr('height', height)
        .attr('viewBox', `0 0 ${Math.min(width, height)} ${Math.min(width, height)}`)
        .attr('preserveAspectRatio', 'xMinYMin');

    const xScale = d3.scaleLinear()
        .domain([
            0, 24
        ])
        .range([
            0, width
        ]);

    const yScale = d3.scaleLinear()
        .domain([
            0, 60
        ])
        .range([
            height, 0
        ]);

    timeStats.selectAll('circle')
        .data(data.drinks)
        .enter()
        .append('circle')
        .attr('cx', d => xScale(parseTime(d.drinks[0].time, 'hour')))
        .attr('cy', d => yScale(parseTime(d.drinks[0].time, 'minute')))
        .attr('r', d => d.drinks[0].amount / 1.2)
        .attr('x', d => xScale(parseTime(d.drinks[0].time, 'hour')))
        .attr('y', d => yScale(parseTime(d.drinks[0].time, 'minute')));

    // display all drinks from each day
    const multipleTimeStats = d3.select('.multiple-time-stats')
        .attr('width', width)
        .attr('height', height)
        .attr('viewBox', `0 0 ${Math.min(width, height)} ${Math.min(width, height)}`)
        .attr('preserveAspectRatio', 'xMinYMin');

    multipleTimeStats.selectAll('g')
        .data(data.drinks)
        .enter()
        .append('g')
        .attr('data-date', d => d.date)
        .selectAll('circle')
        .data(d =>
            // console.log('d', d);
            d.drinks
        )
        .enter()
        .append('circle')
        .attr('r', d => {
            console.log('d', d);

            return d.amount / 1.2;
        })
        .attr('cx', d => xScale(parseTime(d.time, 'hour')))
        .attr('cy', d => yScale(parseTime(d.time, 'minute')))
        .attr('x', d => xScale(parseTime(d.time, 'hour')))
        .attr('y', d => yScale(parseTime(d.time, 'minute')));
};

const renderAnnotated = function renderAnnotated(data) {
    const parseTime_ = d3.timeParse('%H:%M');
    const midnight = parseTime_('00:00');

    const margin = {
        top: 30,
        right: 30,
        bottom: 30,
        left: 30
    };
    const width = 960 - margin.left - margin.right;
    const height = 500 - margin.top - margin.bottom;

    const x = d3.scaleTime()
        .domain([
            midnight, d3.timeDay.offset(midnight, 1)
        ])
        .range([
            0, width
        ]);

    const y = d3.scaleLinear()
        .range([
            height, 0
        ]);

    const svg = d3.select('body').append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`);

    y.domain([
        0, d3.max(data, d => d.rate)
    ]);

    svg.append('g')
        .attr('class', 'axis axis--x')
        .attr('transform', `translate(0,${height})`)
        .call(d3.axisBottom(x)
            .tickFormat(d3.timeFormat('%I %p')));

    svg.append('g')
        .attr('class', 'dots')
        .selectAll('path')
        .data(data)
        .enter().append('path')
        .attr('transform', d => `translate(${x(d.time)},${y(d.rate)})`)
        .attr('d', d3.symbol()
            .size(40));

    //   var tick = svg.append('g')
    //       .attr('class', 'axis axis--y')
    //       .call(d3.axisLeft(y).tickSize(-width))
    //
    //         //   d3.svg.axis()
    //         //   .scale(y)
    //         //   .tickSize(-width)
    //         //   .orient('left'))
    //     .select('.tick:last-of-type');
    //
    //   var title = tick.append('text')
    //       .attr('dy', '.32em')
    //       .text('tweets per hour');
    //
    //   tick.select('line')
    //       .attr('x1', title.node().getBBox().width + 6);
    // // });

    // const type = function type(d) {
    //     d.rate = Number(d.count) / 327 * 60; // January 8 to November 30
    //     d.time = parseTime(d.time);
    //     d.time.setUTCHours((d.time.getUTCHours() + constants.HOURS_IN_DAY - 7) % constants.HOURS_IN_DAY);
    //
    //     return d;
    // };
};

const brewit = function brewit() {
    d3.json('data/data.json', data => {
        console.info('data is loaded', data);
        // http://chimera.labs.oreilly.com/books/1230000000345/ch06.html#_labels
        render(data);
        renderAnnotated(data);
    });

    // https://twitter.com/jashkenas/status/502886697622048768
    // usage: d3.rave(stats.selectAll('rect'));
    d3.rave = function rave(target) {
        d3.timer(elapsedTime => {
            target.style('stroke', d3.hsl(elapsedTime / 10, 1, 0.5)).style('stroke-width', `${Math.sin(t / 100) * 2 + 3}px`);
        });
    };
};

brewit();
